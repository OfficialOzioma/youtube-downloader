# Build AdonisJS
FROM node:16-alpine as builder
# Set directory for all files
WORKDIR /home/node
# Copy over package.json files
COPY package*.json ./
# Install all packages
RUN npm install
# Copy over source code
COPY . .
# Build AdonisJS for production
RUN npm run build --production


# Install prod packages on different step
FROM node:16-alpine as installer
# Set directory for all files
WORKDIR /home/node
# Copy over package.json files
COPY package*.json ./
# Install only prod packages
RUN npm ci --only=production


# Build final runtime container
FROM node:16-alpine
# Set environment variables
ENV NODE_ENV=production
# Disable .env file loading
ENV ENV_SILENT=true
# Set app key at start time
ENV HOST=0.0.0.0
ENV APP_NAME=youtube-downloader
ENV CACHE_VIEWS=true
ENV SESSION_DRIVER=cookie
ENV PORT=3333
ENV APP_KEY=

# Install deps required for this project
RUN apk add --no-cache ffmpeg
# Use non-root user
USER node
# Make directory for app to live in
# It's important to set user first or owner will be root
# For some reason mkdir fails in Gitlab CI
# RUN mkdir -p /home/node/app
# Set working directory
WORKDIR /home/node
# Copy over required files from previous steps
# Copy over built files
COPY --from=builder /home/node/build ./build
# Copy over node_modules
COPY --from=installer /home/node/node_modules ./node_modules
# Copy over package.json files
COPY package*.json ./
# Expose port 3333 to outside world
EXPOSE 3333
# Start server up
CMD [ "node", "./build/server.js" ]
