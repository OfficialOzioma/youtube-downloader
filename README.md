#  YouTube video / music downloader

> AdonisJS V5 learning project that also is somewhat useful

## Goals:

- Dockerize AdonisJS V5
- Stream responses
- Use Gitlab CI for auto build and deploy

## Useful stuff

### Putting it into container

Everything useful related to Dockerizing AdonisJS is in [Dockerfile](https://gitlab.com/McSneaky/youtube-downloader/-/blob/master/Dockerfile) and [.dockerignore](https://gitlab.com/McSneaky/youtube-downloader/-/blob/master/.dockerignore)

Container is built in multiple steps
- First step is installing all packages and building it
- Second step installs only prod packages
- Third step copies built folder from first step and prod packages from 2nd step to final container

### Stream responses

It's all in [routes.ts](https://gitlab.com/McSneaky/youtube-downloader/-/blob/master/start/routes.ts). Idea is video or sound is streamed from YouTube to `ffmpeg` and from there to response.

Sound is always downloaded at highest quality, but video quality might suffer a lot. It's because YouTube keeps video and sound as separate streams. Could use `ffmpeg` to merge highest quality audio and video to single stream and send that back in response, but it requires too much CPU to [host it in the wild](https://yt.ap3k.pro/) for free.

### Gitlab automation

Everything useful is in [.gitlab-ci.yml](https://gitlab.com/McSneaky/youtube-downloader/-/blob/master/.gitlab-ci.yml)

You can line-by-line explanation of it in [my blog](https://mcsneaky.ap3k.pro/posts/docker-autodeploy-with-gitlab/).
