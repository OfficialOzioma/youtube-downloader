/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import ytdl from 'ytdl-core'
import ffmpeg from 'fluent-ffmpeg'
import Stream from 'stream'

Route.on('/').render('welcome')

// Handle download
Route.post('/', async ({ request, response, logger }) => {
  const { format, link } = request.post()

  // Get video title from Youtube
  const meta = await ytdl.getInfo(link)

  // Get rid of special characters in title
  const title = meta.videoDetails.title.normalize('NFD').replace(/[\u0300-\u036f]/g, '')
  logger.info(meta.videoDetails.title)

  // Set content headers to download stream as a file
  response.header('content-disposition', `attachment; filename="${encodeURIComponent(title)}.${format}"`)

  // Check if user wanted video or music only
  if (format === 'mp4') {
    // Start streaming mp4 video of requested link
    return response.stream(ytdl(link, {
      filter: format => format.container === 'mp4',
    }))
  }

  // Initialize stream for YT download
  const ytdlStream = ytdl(link, {
    filter: 'audioonly',
    quality: 'highestaudio',
  })

  // Make temp passthro stream to pass ffmpeg output to response
  const stream = new Stream.PassThrough()

  // Start converting Youtube stream to mp3
  ffmpeg(ytdlStream)
    .toFormat('mp3')
    .on('end', () => {
      logger.info('Converted: ' + title)
    })
    .on('error', (error) => {
      logger.error(error)
    })
    // Pipe conversion output to temp pass through stream
    .pipe(stream)

  // Send stream back to client
  response.stream(stream)
})
